# Solução do Desafio frontend - Responsividade - Squad Homem de Ferro

Esta é a solução para o [Desafio frontend - Responsividade - Squad Homem de Ferro](https://github.com/airlonfilho/desafio-responsividade). 

## Conteúdo

- [Solução do Desafio frontend - Responsividade - Squad Homem de Ferro](#solução-do-desafio-frontend---responsividade---squad-homem-de-ferro)
  - [Conteúdo](#conteúdo)
  - [Visão Geral](#visão-geral)
    - [O desafio](#o-desafio)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [Meu processo](#meu-processo)
    - [Feito com](#feito-com)
    - [Coisas que aprendi](#coisas-que-aprendi)
    - [Desenvolvimento contínuo](#desenvolvimento-contínuo)
    - [Referências úteis](#referências-úteis)
  - [Autor](#autor)
  - [Agradecimentos](#agradecimentos)

## Visão Geral

### O desafio

Construir essa landing page e deixá-la o mais próximo possível do design.

Seus usuários devem ser capazes de:

- Veja o layout ideal para o site, dependendo do tamanho da tela do dispositivo
- Veja os estados de foco para todos os elementos interativos na página

### Screenshot

[![](captures/capture-desktop-preview.png)](captures/capture-desktop.png)
[![](captures/capture-mobile-preview.png)](captures/capture-mobile.png)

### Links

- Código fonte da Solução: [gitlab.com/homem-de-ferro/lab/wellington-santos/desafio-responsividade](https://gitlab.com/homem-de-ferro/lab/wellington-santos/desafio-responsividade)
- Site: [desafio-reponsividade-hulkbuster.netlify.app](https://desafio-reponsividade-hulkbuster.netlify.app)

## Meu processo

### Feito com

- Marcações Semânticas do HTML5
- Propriedades CSS personalizadas (variáveis)
- Media Query
- Flexbox
- Mobile-first

### Coisas que aprendi

Como utilizar valores negativos na margem para dar um efeito de um elemento sobreposto.

Outra coisa foi colocar um "marcador de lista" personalizado para os itens no rodapé.

como no código abaixo:

```html
<address> 
  <a href="tel:+15431234567">+1-543-123-4567</a>
  <a href="mailto:exemple@fylo.com">exemple@fylo.com</a>
</address>
```
A idéia foi colocar os elementos internos à tag address para serem tradados como itens de uma lista, remover o marcador, e dar um espaço de 40px no ínicio
```css
address * {
    display: list-item;
    list-style: '';
    margin-left: 40px;
}
```
Uso a pseudo elemento ::before para posicionar o novo marcador, note que a propriedade "content" ainda não foi colocada, aqui é apenas para colocar as regras que todos os marcadores seguirão.
```css
address *::before {
    left: 8px;
    position: absolute;
}
```
Como cada elemento possuirá um marcador diferente eu coloca um content para cada um deles especificamente:
```css
footer section address :nth-child(1)::before {
    content: url('images/icon-phone.svg');
}
footer section address :nth-child(2)::before {
    content: url('images/icon-email.svg');
}
```

### Desenvolvimento contínuo

A responsividade é importante e esse foi um projeto, que embora simples, necessitou de pesquisa para ser concluído, como o foco deste projeto é a responsividade, não fiz um teste para saber se a acessibilidade está boa, acho que seria uma boa área para pesquisa futura se o site consegue entregar um conteúdo acessível e suficientemente amigável para leitores de tela.

### Referências úteis

- [MDN_](https://developer.mozilla.org/pt-BR/) - Uma das melhores fontes de documentação de tecnologias web principalmente HTML, CSS e JavaScript.
- [Modular Scale](https://www.modularscale.com) - Uma ferramenta para ajudar criar uma escala de tamanhos tipografia para ser usada como referência nos elementos do projeto. 

## Autor

- Website - [Wellington Oliveira](https://gitlab.com/wellington.oliveira.dev)


## Agradecimentos

Deixo um agradecimento ao nosso Project Owner [Airlon](https://github.com/airlonfilho) pelo desafio, e pelas explicações sobre responsividade, me deram muitas ideias de como aplicar melhor os conceitos que tinha aprendido para o projeto anterior.
